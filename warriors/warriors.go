package warriors

import (
	"strings"
)

func Count(image string) int {
	var count int
	arr := strings.Split(image, "\n")

	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[i]); j++ {
			if i == 0 {
				if arr[i][j] == '1' {
					if j == 0 {
						count++
					} else {
						if arr[i][j-1] == '0' {
							count++
						}
					}
				}
			} else {
				if arr[i][j] == '1' {
					switch j {
					case 0:
						if arr[i-1][j] == '0' && arr[i-1][j+1] == '0' {
							count++
						}
					case len(arr[i]) - 1:
						if arr[i-1][j-1] == '0' && arr[i-1][j] == '0' && arr[i][j-1] == '0' {
							count++
						}
					default:
						if arr[i-1][j-1] == '0' && arr[i-1][j] == '0' && arr[i-1][j+1] == '0' && arr[i][j-1] == '0' {
							count++
						}
					}
				}
			}
		}
	}

	return count
}
