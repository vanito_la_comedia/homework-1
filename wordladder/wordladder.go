package wordladder

func WordLadder(from string, to string, dic []string) int {
	if len(from) != len(to) {
		return 0
	}
	fI := -1
	tI := -1
	for i, v := range dic {
		if v == from {
			fI = i
		}
		if v == to {
			tI = i
		}
	}
	if tI == -1 { // to not in dic
		return 0
	}
	if fI == -1 { // from not in dic, add it to the end
		fI = len(dic)
		dic = append(dic, from)
	}

	neighbours := findNeighbours(dic)
	if len(neighbours[tI]) == 0 {
		return 0 // no neighbours found
	}

	var visited = make([]bool, len(dic))
	var way int
	var res []int
	findNext(fI, tI, neighbours, way, visited, &res)
	result := MinIntSlice(res)
	return result
}

func MinIntSlice(v []int) int {
	if len(v) == 0 {
		return 0
	}
	var m = v[0]
	for i := 1; i < len(v); i++ {
		if v[i] < m {
			m = v[i]
		}
	}
	return m
}

func findNext(srcInd int, dstInd int, neigh [][]int, way int, visited []bool, res *[]int) {
	visited[srcInd] = true
	way++
	for i := 0; i < len(neigh[srcInd]); i++ {
		checkID := neigh[srcInd][i]
		if checkID == dstInd {
			way++
			*res = append(*res, way)
			visited[checkID] = false
			break
		}
		if visited[checkID] {
			continue
		}
		findNext(checkID, dstInd, neigh, way, visited, res)
	}
	visited[srcInd] = false
}

func findNeighbours(dic []string) [][]int {
	var neigh [][]int
	for i := range dic {
		var n []int
		for j := range dic {
			if i == j {
				continue // this is original word
			}
			if dist(dic[i], dic[j]) == 1 {
				n = append(n, j)
			}

		}
		//fmt.Println(n)
		neigh = append(neigh, n)
	}
	return neigh
}

func dist(src string, dst string) int {
	var res int
	for i := range src {
		if src[i] != dst[i] {
			res++
		}
	}
	return res
}
