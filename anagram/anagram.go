package anagram

import (
	"strings"
)

// convert string to dictionary with counter for each rune
func strToDict(str string) map[rune]uint {
	runeStr := []rune(str)
	dict := make(map[rune]uint)
	for i := range runeStr {
		dict[runeStr[i]] += 1
	}
	return dict
}

func compareDicts(dict1 map[rune]uint, dict2 map[rune]uint) bool {
	//check len first
	if len(dict1) != len(dict2) {
		return false
	}

	for k, v1 := range dict1 {
		if v2, found := dict2[k]; !found || v1 != v2 {
			return false
		}
	}
	return true
}

func isAnagram(string1 string, string2 string) bool {
	checkDict := strToDict(string1)
	wordDict := strToDict(string2)
	return compareDicts(wordDict, checkDict)
}

func FindAnagrams(dictionary []string, word string) (result []string) {
	wordStr := strings.ReplaceAll(strings.ToLower(word), " ", "")
	for i := range dictionary {
		checkStr := strings.ReplaceAll(strings.ToLower(dictionary[i]), " ", "")
		if wordStr == checkStr { //strings without spaces are equal, so skip it
			continue
		}
		if isAnagram(checkStr, wordStr) {
			result = append(result, dictionary[i])
		}
	}
	return result
}
