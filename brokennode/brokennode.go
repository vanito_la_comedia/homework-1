package brokennode

import "math/bits"

func FindBrokenNodes(brokenNodes int, reports []bool) string {

	res := make([]byte, len(reports))

	// start-conf: broken nodes come first
	var conf uint64 = 1<<uint8(brokenNodes) - 1
	var max uint64 = 1 << uint8(len(reports))

out:
	for {
		for !check(conf, reports) {
			if conf = nextPerm(conf); conf >= max {
				break out
			}
		}

		merge(res, conf)
		if conf = nextPerm(conf); conf >= max {
			break
		}
	}

	return string(res)
}

const (
	B uint64 = 1
	W uint64 = 0
)

func merge(result []byte, conf uint64) {

	chr := func(c uint64) byte {
		if c&1 == B {
			return 'B'
		}
		return 'W'
	}

	for i := range result {
		switch {
		case result[i] == 0:
			result[i] = chr(conf)
		case result[i] != chr(conf):
			result[i] = '?'
		}
		conf >>= 1
	}
}

func check(conf uint64, reports []bool) bool {

	c := conf | ((conf & 1) << uint8(len(reports)))

	for i := range reports {
		if c&1 == B {
			c >>= 1
			continue
		}

		want := B
		if reports[i] {
			want = W
		}

		c >>= 1
		if c&1 != want {
			return false
		}
	}

	return true
}

func nextPerm(v uint64) uint64 {
	t := v | (v - 1)
	return (t + 1) | ((^t&-^t - 1) >> (uint8(bits.TrailingZeros64(v)) + 1))
}
